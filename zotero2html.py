import os
import re
from bibtexparser.bparser import BibTexParser
import bibtexparser.customization as btc
from jinja2 import Environment, DictLoader, select_autoescape


def parse_biblatex(bib_file_name):
    def customizations(record):
        "Example copied from the bibtexparser documentation"
        record = btc.type(record)
        record = btc.author(record)
        record = btc.editor(record)
        record = btc.journal(record)
        record = btc.keyword(record)
        record = btc.link(record)
        record = btc.page_double_hyphen(record)
        record = btc.doi(record)
        return record

    parser = BibTexParser()
    parser.customization = customizations
    parser.ignore_nonstandard_types = False
    parser.homogenise_fields = False
    parser.common_strings = False

    with open(bib_file_name, 'rt') as bib_file:
        bib = parser.parse_file(bib_file)

    return bib


def main(bib_file_name):
    """
    Turn a BibLaTex export from Zotero into a HTML page with links to the
    pdfs that are linked by Zotero
    """
    # Parse the bib file
    bib = parse_biblatex(bib_file_name)

    # Massage data into output format
    with_other = []
    with_pdf = []
    no_files = []
    for entry in bib.entries:
        entry = entry.copy()
        if 'file' not in entry:
            no_files.append(entry)
            continue

        files = entry['file'].split(';')
        has_pdf = False
        for info in files:
            # Split by a colon that is not escaped by a backslash
            _name, path, mime_type = re.split(r'(?<!\\):', info)
            if mime_type == 'application/pdf':
                has_pdf = True
                entry['MAIN_FILE'] = path
            entry.setdefault('FILES', []).append(path)

        if has_pdf:
            with_pdf.append(entry)
        else:
            with_other.append(entry)

    # Page title
    title = os.path.basename(bib_file_name)
    title = os.path.splitext(title)[0].replace('_', ' ')

    # Some meta information
    meta = (
        'Found %d entries with PDF + %d entries with other file + '
        '%d entries without any file = %d entries total.'
        % (
            len(with_pdf),
            len(with_other),
            len(no_files),
            len(with_pdf) + len(with_other) + len(no_files),
        )
    )

    # Sort key for entries
    def key(e):
        return (e.get('author', [])[0], e.get('date', ''))

    # Produce html
    out = {
        'page_title': title,
        'meta': meta,
        'pdf_entries': sorted(with_pdf, key=key),
        'otherfile_entries': sorted(with_other, key=key),
        'nofile_entries': sorted(no_files, key=key),
    }
    env = Environment(loader=DictLoader(TEMPLATES), autoescape=select_autoescape(['html', 'xml']))
    env.filters['latex_cleanup'] = latex_cleanup
    env.filters['date_cleanup'] = date_cleanup
    template = env.get_template('base')
    html = template.render(**out)
    with open(bib_file_name + '.html', 'wt') as html_file:
        html_file.write(html)

    # import pprint
    # pprint.pprint(with_pdf[0])
    print(meta)


def latex_cleanup(text):
    return text.replace('{', '').replace('}', '').replace('\\&', '&').replace('--', '&ndash;')


def date_cleanup(text):
    return text.split('-')[0]


TEMPLATES = {
    'base': """
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>{{ page_title }}</title>
    <style>
table.bib-entries {
    border-collapse: collapse;
    width: 100%;
}
table.bib-entries tr:nth-child(odd) {
    background: #c4d8ed;
}
table.bib-entries td.authors {
    padding: 0;
    width: 30%;
}
table.bib-entries td.authors ul {
    display: inline;
    margin: 0;
    padding: 0;
}
table.bib-entries td.authors li {
    display: inline-block;
    margin: 3pt;
    padding: 2pt;
    border-radius: 3px;
    background: rgba(255, 186, 84, 0.3);
}
table.bib-entries td.date {
    padding: 0 1.5em;
}
    </style>
</head>
<body>

{% macro show_entries(entries, cls) -%}
    <table class="bib-entries {{cls}}">
    <tbody>
    {% for entry in entries %}
        <tr>
            <td class="authors">
                <ul>
                {% for author in entry.get('author', []) %}
                    <li>{{ author | latex_cleanup }}</li>
                {% endfor %}
                </ul>
            </td>
            <td class="date">
                {{ entry.get('date', '') | date_cleanup }}
            </td>
            <td class="title">
                {# -- Name of the entry ------------------------------------- #}
                {% if 'MAIN_FILE' in entry %}
                    <a href="{{entry['MAIN_FILE'] | urlencode}}">
                    {{ entry.get('title', 'NO_TITLE') | latex_cleanup }}</a>,
                {% else %}
                    {{ entry.get('title', 'NO_TITLE') | latex_cleanup }},
                {% endif %}

                {# -- Name of journal, book, conference etc ----------------- #}
                {{ show_key(entry, 'journaltitle', 'em') }}
                {{ show_key(entry, 'booktitle', 'em') }}
                {{ show_key(entry, 'volume', 'span') }}
                {{ show_key(entry, 'number', 'span') }}
                {{ show_key(entry, 'pages', 'span') }}

                {% if entry['ENTRYTYPE'] == 'thesis' and 'institution' in entry%}
                    <span class="institution">{{ entry['institution'] | latex_cleanup }}</span>
                {% endif %}

                {# -- Type of entry (thesis etc) ---------------------------- #}
                {% if entry['ENTRYTYPE'] != 'article' %}
                    ({{ entry['ENTRYTYPE'] }})
                {% endif %}
            </td>
        </tr>
    {% endfor %}
    </tbody>
    </table>
{%- endmacro %}

{% macro show_key(entry, key, type='span') -%}
    {% if key in entry %}
        <{{type}} class="volume">{{entry[key]| latex_cleanup}}</{{type}}>,
    {% endif %}
{%- endmacro %}

    <h1>{{ page_title }}</h1>
    <p>{{ meta }}</p>

    <h2>Entries with PDF file attachements</h2>
    {{ show_entries(pdf_entries, 'pdf') }}

    <h2>Entries with other files attached</h2>
    {{ show_entries(otherfile_entries, 'other') }}

    <h2>Entries with no files attached</h2>
    {{ show_entries(nofile_entries, 'none') }}

</body>
</html>
"""
}

if __name__ == '__main__':
    import sys

    bib_file_name = sys.argv[1]
    main(bib_file_name)
