ZoteroToHTML
============

From Zotero, export your library to biblatex and select to also export
files. This script will parse the bib file and make an html file with
clickable links to the pdf files and also show which files are missing
pdfs (to help you complete your library).

The goal is to make your Zotero library available offline and without the
Zotero client. It is meant for read only usage and can be used as a 
secondary future-proof backup of a Zotero library. You should of course
also backup the library itself if you want to be able to update it and
add further references in the future.

Requires:

- Python 3
- `python-bibtexparser <https://github.com/sciunto-org/python-bibtexparser>`_
- `jinja2 <http://jinja.pocoo.org/>`_

Usage::

    python3 zotero2html.py ~/Backup/Zotero_export_2018-07-26/Zotero_export_2018-07-26.bib

This creates a file ``~/Backup/Zotero_export_2018-07-26/Zotero_export_2018-07-26.bib.html``
which can be searched (``Ctrl+F`` in your browser) and browsed to find
pdf files.


Copyright and license
---------------------

ZoteroToHTML is copyright Tormod Landet, 2018. ZoteroToHTML is licensed
under the Apache 2.0 license, a  permissive free software license
compatible with version 3 of the GNU GPL.
